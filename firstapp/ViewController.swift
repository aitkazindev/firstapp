//
//  ViewController.swift
//  firstapp
//
//  Created by Ibrahim on 09.04.2021.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textfield: UITextField!
    @IBOutlet weak var secondtextfield: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func touched(_ sender: Any) {
        let a = Int(textfield.text!) ?? 0
        let b = Int(secondtextfield.text!) ?? 0
        let sum = a + b
        label.text = "sum = \(sum)"
    }
    
    @IBAction func minus(_ sender: Any) {
        let a = Int(textfield.text!) ?? 0
        let b = Int(secondtextfield.text!) ?? 0
        let minus = a - b
        label.text = "minus = \(minus)"
    }
    @IBAction func mult(_ sender: Any) {
        let a = Int(textfield.text!) ?? 0
        let b = Int(secondtextfield.text!) ?? 0
        let mult = a * b
        label.text = "multiple = \(mult)"
    }
    @IBAction func devide(_ sender: Any) {
        let a = Double(textfield.text!) ?? 0
        let b = Double(secondtextfield.text!) ?? 0
        if b == 0.0{
            label.text = "error"
        }else {
            let mult = a / b
            label.text = "multiple = \(mult)"
        }
        
        
    }
    
}

